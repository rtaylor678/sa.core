﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Text;

namespace Meta.Numerics.Series
{
    /// <summary>
    /// Geometric Series functions for Power = 2;
    /// http://en.wikipedia.org/wiki/1_%2B_2_%2B_4_%2B_8_%2B_%C2%B7_%C2%B7_%C2%B7
    /// </summary>
    public static class PartialGeometric
    {
        private const Int32 r = 2;

        /// <summary>
        /// Verify if a number was used in the partial calculation of a geometric series.
        /// Given the value 5, numbers 1 and 4 are contained in 5.
        /// </summary>
        /// <param name="GeometricNumber">The number to verify if it is contained in the geometric series.</param>
        /// <param name="PartialAggregate">The partial geometric series aggregate</param>
        /// <returns>Boolean</returns>
        public static Boolean Contains(Int32 GeometricNumber, Int32 PartialAggregate)
        {
            if (GeometricNumber > 0)
                return (PartialAggregate % (r * GeometricNumber)) >= GeometricNumber;
            else
                if ((GeometricNumber == 0) && (PartialAggregate == 0))
                    return true;
                else
                    return false;
        }

        public static Int32 NextNumber(Int32 GeometricNumber, Int32 PartialAggregate, Int32 dir)
        {
            if (Contains(GeometricNumber, PartialAggregate)) return GeometricNumber;
            else
            {
                Int32 k = Convert.ToInt32(Math.Log(GeometricNumber) / Math.Log(r));
                GeometricNumber = Convert.ToInt32(Math.Pow(r, k + Convert.ToInt32(dir)));
                if (GeometricNumber != 0) return NextNumber(GeometricNumber, PartialAggregate, dir);
                else return 0;
            }
        }

        public enum Direction
        {
            Previous = -1,
            Next = 1
        }
    }
}
