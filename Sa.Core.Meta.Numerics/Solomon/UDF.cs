﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

using Meta.Numerics.Statistics;
using Meta.Numerics.Statistics.Distributions;

public partial class UserDefinedFunctions
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="nu">Double ν (Degrees of Freedom)</param>
    /// <param name="P"></param>
    /// <returns>SqlDouble: Ciritcal Value for the Student T Distribution</returns>
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDouble Student_InverseLeftProbability(Double nu, Double P)
    {
        P = (100.0 - P) / 200.0;
        StudentDistribution d = new StudentDistribution(nu);
        return - d.InverseLeftProbability(P);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="nu">Double ν (Degrees of Freedom)</param>
    /// <param name="C">Double: Critical Value</param>
    /// <returns>SqlDouble: Aplha (α)</returns>
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDouble Student_Critical(Double nu, Double C)
    {
        StudentDistribution d = new StudentDistribution(nu);
        return d.RightProbability(C);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="z"></param>
    /// <returns>SqlDouble Probability</returns>
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDouble Norm(Double z)
    {
       NormalDistribution d = new NormalDistribution();
       return d.LeftProbability(z);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="P"></param>
    /// <returns>SqlDouble Z-Test</returns>
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDouble Norm_Z(Double P)
    {
        P = P / 100.0;
        NormalDistribution d = new NormalDistribution();
        return d.InverseLeftProbability(P);
    }

    [Obsolete, Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDouble fischer(Double nu1, Double nu2)
    {
        FisherDistribution d = new FisherDistribution(nu1, nu2);
        return 1.0;
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDouble Pearson(Int32 n, Double P)
    {
        PearsonRDistribution d = new PearsonRDistribution(n);
        return d.InverseLeftProbability(P);
    }

    [Obsolete, Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDouble OneVirateSample()
    {
        //TestResult r;
        Double d;
        Meta.Numerics.Interval i;

        Sample s = new Sample();
        d = s.Mean;
        d = s.Median;
        d = s.StandardDeviation;
        d = s.Variance;
        d = s.Count;
        d = s.Maximum;
        d = s.Minimum;
        
        i = s.InterquartileRange;
        d = i.LeftEndpoint;
        d = i.Midpoint;
        d = i.RightEndpoint;
        d = i.Width;

        return 1.0;
    }

    [Obsolete, Microsoft.SqlServer.Server.SqlFunction]
    public static SqlDouble BiVirateSample()
    {
        TestResult r;

        BivariateSample s = new BivariateSample();
        r = s.SpearmanRhoTest();
        r = s.PearsonRTest();
        r = s.KendallTauTest();
        r = s.PairedStudentTTest();
  
        return 1.0;
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlBoolean Contains(Int32 GeometricNumber, Int32 PartialAggregate)
    {
        return Meta.Numerics.Series.PartialGeometric.Contains(GeometricNumber, PartialAggregate);
    }

    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlInt32 PreviousNumber(Int32 GeometricNumber, Int32 PartialAggregate, Int32 Dir)
    {
        return Meta.Numerics.Series.PartialGeometric.NextNumber(GeometricNumber, PartialAggregate, Dir);
    }

};

