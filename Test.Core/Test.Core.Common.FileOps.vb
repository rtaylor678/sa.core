﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports System.Security.AccessControl
Imports Microsoft.VisualBasic.FileIO

<TestClass()>
Public Class FileOps_Test

    Private testContextInstance As TestContext

    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = value
        End Set
    End Property

    <TestMethod()>
    Public Sub CopyDeleteFile()

        Const NequePorro As String = "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit."
        Dim sTempFile As String = TestContext.DeploymentDirectory + "\sNequePorroCDF.txt"
        Dim tTempFile As String = TestContext.DeploymentDirectory + "\tNequePorroCDF.txt"

        Dim b As Boolean

        System.IO.File.WriteAllText(sTempFile, NequePorro, System.Text.Encoding.ASCII)

        b = Sa.Core.FileOps.CopyFile(sTempFile, tTempFile)
        Assert.IsTrue(b, "Sa.Core.OpsFile.CopyFile")
        Assert.IsTrue(System.IO.File.Exists(tTempFile), "Sa.Core.OpsFile.CopyFile")

        b = Sa.Core.FileOps.DeleteFile(sTempFile)
        Assert.IsTrue(b, "Sa.Core.OpsFile.DeleteFile (D.0)")
        Assert.IsFalse(System.IO.File.Exists(sTempFile), "Sa.Core.OpsFile.CopyFile (D.1)")

        Dim fs As FileSecurity = System.IO.File.GetAccessControl(tTempFile)
        fs.SetAccessRuleProtection(True, False)

        Dim t As System.Type = GetType(System.Security.Principal.NTAccount)

        For Each r As FileSystemAccessRule In fs.GetAccessRules(True, True, t)
            fs.RemoveAccessRule(r)
        Next r

        System.IO.File.SetAttributes(tTempFile, IO.FileAttributes.ReadOnly)
        System.IO.File.SetAccessControl(tTempFile, fs)

        b = Sa.Core.FileOps.DeleteFile(tTempFile)
        Assert.IsFalse(b, "Sa.Core.OpsFile.DeleteFile (D.2)")

        Dim d As String = System.Environment.UserDomainName
        Dim u As String = System.Environment.UserName
        Dim n As String = d + "\" + u

        fs.AddAccessRule(New FileSystemAccessRule(n, FileSystemRights.FullControl, AccessControlType.Allow))

        System.IO.File.SetAccessControl(tTempFile, fs)
        System.IO.File.SetAttributes(tTempFile, IO.FileAttributes.Normal)

        b = Sa.Core.FileOps.DeleteFile(tTempFile)
        Assert.IsTrue(b, "Sa.Core.OpsFile.DeleteFile (D.3)")
        Assert.IsFalse(System.IO.File.Exists(tTempFile), "Sa.Core.OpsFile.CopyFile (D.4)")

    End Sub

    <TestMethod()>
    Public Sub VerifyFile()

        Const NequePorro As String = "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit."
        Const sTempFile As String = "sNequePorroVF.txt"
        Const tTempFile As String = "tNequePorroVF.txt"

        Dim b As Boolean = Nothing

        System.IO.File.WriteAllText(sTempFile, NequePorro, System.Text.Encoding.ASCII)
        System.IO.File.WriteAllText(tTempFile, NequePorro + "..", System.Text.Encoding.ASCII)

        b = Sa.Core.FileOps.VerifyFile(sTempFile, sTempFile)
        Assert.IsTrue(b, "Sa.Core.OpsFile.VerifyFile (VerifyFile failed)")

        b = Sa.Core.FileOps.VerifyFile(sTempFile, tTempFile)
        Assert.IsFalse(b, "Sa.Core.OpsFile.VerifyFile (VerifyFile failed)")

        System.IO.File.Delete(sTempFile)
        System.IO.File.Delete(tTempFile)

        Assert.IsFalse(System.IO.File.Exists(sTempFile), "Sa.Core.OpsFile.VerifyFile (D.0)")
        Assert.IsFalse(System.IO.File.Exists(tTempFile), "Sa.Core.OpsFile.VerifyFile (D.1)")

    End Sub

    <TestMethod(), DeploymentItem("..\Test Files", "Test Files")>
    Public Sub CopyDeleteFolder()

        Dim PathSource As String = TestContext.DeploymentDirectory + "\Test Files\"
        Dim PathTarget As String = TestContext.DeploymentDirectory + "\CopyDeleteFolder\"

        Dim b As Boolean = False

        b = Sa.Core.FileOps.CopyFolder(PathSource, PathTarget)
        Assert.IsTrue(b, "Sa.Core.FileOps.CopyFolder (C.0)")
        Assert.IsTrue(System.IO.Directory.Exists(PathTarget), "Sa.Core.FileOps.CopyFolder (C.1)")

        b = Sa.Core.FileOps.DeleteFolder(PathTarget)
        Assert.IsTrue(b, "Sa.Core.FileOps.DeleteFolder (D.0)")
        Assert.IsFalse(System.IO.Directory.Exists(PathTarget), "Sa.Core.FileOps.DeleteFolder (D.1)")

    End Sub

    <TestMethod()>
    Public Sub VerifyFolder()

        Dim s As String = Nothing
        Dim t As String = Nothing
        Dim c As Boolean = Nothing

        s = TestContext.DeploymentDirectory
        t = TestContext.DeploymentDirectory

        c = Sa.Core.FileOps.VerifyFolder(s, t)
        Assert.IsTrue(c, "Sa.Core.OpsFile.VerifyFolder")

        s = TestContext.DeploymentDirectory
        t = TestContext.DeploymentDirectory + "\In"

        c = Sa.Core.FileOps.VerifyFolder(s, t)
        Assert.IsFalse(c, "Sa.Core.OpsFile.VerifyFolder")

    End Sub

    <TestMethod()>
    Public Sub VerifySourceFiles()

        Const NequePorro As String = "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit."
        Const rTempFile As String = "rNequePorroVSF.txt"
        Const sTempFile As String = "sNequePorroVSF.txt"
        Const tTempFile As String = "tNequePorroVSF.txt"
        Const SHA512Hash As String = "SHA512|CF804ECD6EE242FE35304F19DFEE84568F9E8E00F03C1C17C3E605A709425DCB0C0FCDF30598B4A607962E3F60C2A7B541A188805BD55906294E0F2ED14D6B13"
        Const SHA512File As String = "SHA512.txt"

        Dim HashFile As String = Nothing
        Dim sTempPath As String = TestContext.DeploymentDirectory + "\VerifySourceFiles\"

        System.IO.Directory.CreateDirectory(sTempPath)
        System.IO.File.WriteAllText(sTempPath + rTempFile, NequePorro, System.Text.Encoding.ASCII)
        System.IO.File.WriteAllText(sTempPath + sTempFile, NequePorro, System.Text.Encoding.ASCII)
        System.IO.File.WriteAllText(sTempPath + tTempFile, NequePorro, System.Text.Encoding.ASCII)

        Dim b As Boolean = Nothing

        HashFile = rTempFile + "|" + SHA512Hash + ControlChars.NewLine +
            sTempFile + "|" + "SHA512|00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" + ControlChars.NewLine +
            tTempFile + "|" + SHA512Hash + ControlChars.NewLine

        System.IO.File.WriteAllText(SHA512File, HashFile, System.Text.Encoding.ASCII)
        b = Sa.Core.FileOps.VerifySourceFiles(SHA512File, sTempPath)
        Assert.IsFalse(b, "Sa.Core.OpsFile.CheckSum (Invalid Checksum)")

        HashFile = rTempFile + "|" + SHA512Hash + ControlChars.NewLine +
                    sTempFile + "|" + SHA512Hash + ControlChars.NewLine +
                    tTempFile + "|" + SHA512Hash

        System.IO.File.WriteAllText(SHA512File, HashFile, System.Text.Encoding.ASCII)
        b = Sa.Core.FileOps.VerifySourceFiles(SHA512File, sTempPath)
        Assert.IsTrue(b, "Sa.Core.OpsFile.CheckSum (Checksum Failed)")

        HashFile = rTempFile + "|" + SHA512Hash + ControlChars.NewLine +
                    sTempFile + "|" + ControlChars.NewLine +
                    tTempFile + "|" + SHA512Hash + ControlChars.NewLine

        System.IO.File.WriteAllText(SHA512File, HashFile, System.Text.Encoding.ASCII)
        b = Sa.Core.FileOps.VerifySourceFiles(SHA512File, sTempPath)
        Assert.IsTrue(b, "Sa.Core.OpsFile.CheckSum - No Check (Checksum Failed)")

    End Sub

End Class
