﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

<TestClass()> _
Public Class Log_Test

    Private testContextInstance As TestContext

    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = value
        End Set
    End Property

    Private Const sLog As String = "Application"

    <TestMethod()>
    Public Sub ToSystem()

        Dim EventData As String = "Testing writing to the event log"
        Dim EventID As System.Int32 = 0

        Dim LogType As New List(Of EventLogEntryType)

        ' LogType.Add(New EventLogEntryType())
        LogType.Add(EventLogEntryType.Error)
        LogType.Add(EventLogEntryType.FailureAudit)
        LogType.Add(EventLogEntryType.Information)
        LogType.Add(EventLogEntryType.SuccessAudit)
        LogType.Add(EventLogEntryType.Warning)

        For Each lt As EventLogEntryType In LogType

            Try

                Sa.Core.Log.Write(EventData + ControlChars.CrLf + lt.ToString, lt, False, sLog, EventID)
                Assert.IsTrue(True, "WriteLog.ToSystem (1.0)")

            Catch ex As Exception

                Assert.IsTrue(False, "WriteLog.ToSystem")

            End Try

        Next lt

        Sa.Core.Log.Write(EventData + ControlChars.CrLf + EventLogEntryType.Error.ToString, New EventLogEntryType(), False, sLog, EventID)
        Assert.IsTrue(True, "WriteLog.ToSystem (1.1)")

        Sa.Core.Log.Write(EventData + ControlChars.CrLf + EventLogEntryType.Error.ToString, EventLogEntryType.Error, True, sLog, EventID)
        Assert.IsTrue(True, "WriteLog.ToSystem (1.1)")

        Sa.Core.Log.Write(EventData + ControlChars.CrLf + EventLogEntryType.Error.ToString, EventLogEntryType.Error, False, "Modifier", EventID)
        Assert.IsTrue(True, "WriteLog.ToSystem (1.1)")

        Sa.Core.Log.Write(EventData + ControlChars.CrLf + EventLogEntryType.Error.ToString, EventLogEntryType.Error, False, sLog, -9)
        Assert.IsTrue(True, "WriteLog.ToSystem (1.1)")

    End Sub

    <TestMethod()>
    Public Sub ExceptionToString()

        Dim z As New List(Of String())

        z.Add({Sa.Core.Log.ExceptionToString(EventLogEntryType.Error, True), "An Error"})
        z.Add({Sa.Core.Log.ExceptionToString(EventLogEntryType.FailureAudit, True), "A failure audit"})
        z.Add({Sa.Core.Log.ExceptionToString(EventLogEntryType.Information, True), "An informational event"})
        z.Add({Sa.Core.Log.ExceptionToString(EventLogEntryType.SuccessAudit, True), "A success audit"})
        z.Add({Sa.Core.Log.ExceptionToString(EventLogEntryType.Warning, True), "A warning"})
        z.Add({Sa.Core.Log.ExceptionToString(New EventLogEntryType, True), "An unknown error type"})

        z.Add({Sa.Core.Log.ExceptionToString(EventLogEntryType.Error, False), "an Error"})
        z.Add({Sa.Core.Log.ExceptionToString(EventLogEntryType.FailureAudit, False), "a failure audit"})
        z.Add({Sa.Core.Log.ExceptionToString(EventLogEntryType.Information, False), "an informational event"})
        z.Add({Sa.Core.Log.ExceptionToString(EventLogEntryType.SuccessAudit, False), "a success audit"})
        z.Add({Sa.Core.Log.ExceptionToString(EventLogEntryType.Warning, False), "a warning"})
        z.Add({Sa.Core.Log.ExceptionToString(New EventLogEntryType, False), "an unknown error type"})

        For Each s As String() In z

            Assert.AreEqual(s(0), s(1), "WriteLog.ExceptionToString " + s(1))

        Next s

    End Sub

    <TestMethod()>
    Public Sub DisplayEntry()

        Dim e As String = "Message box body"
        Dim c As String = "dialog box caption"

        Dim LogType As New List(Of EventLogEntryType)

        LogType.Add(New EventLogEntryType())
        LogType.Add(EventLogEntryType.Error)
        LogType.Add(EventLogEntryType.FailureAudit)
        LogType.Add(EventLogEntryType.Information)
        LogType.Add(EventLogEntryType.SuccessAudit)
        LogType.Add(EventLogEntryType.Warning)

        For Each lt As EventLogEntryType In LogType

            Try

                Sa.Core.Log.DisplayEntry(e, c, lt)
                Assert.IsTrue(True, "WriteLog.ToSystem")

            Catch ex As Exception

                Assert.IsTrue(False, "WriteLog.ToSystem")

            End Try

        Next lt

    End Sub

    <TestMethod()>
    Public Sub WriteError()

        Dim x As New System.IO.IOException

        Sa.Core.Log.WriteError(sLog, x)
        Assert.IsTrue(True, "WriteSystemLogError")

    End Sub

End Class
