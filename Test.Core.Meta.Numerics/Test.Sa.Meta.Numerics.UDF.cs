﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data.SqlTypes;

namespace TestSolomon
{
    
    /// <summary>
    ///This is a test class for UserDefinedFunctions and is intended
    ///to contain all UserDefinedFunctions Unit Tests
    ///</summary>
    [TestClass()]
    public class UDF
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for calc
        ///</summary>
        [TestMethod()]
        public void UDF_Student_InverseLeftProbability()
        {
            SqlDouble expected;
            SqlDouble actual;

            Double e;
            Double n;
            Double p;
            Int32 r; 

            e = 3.078;
            n = 1.0;
            p = 80.0;
            r = 3;

            expected = new SqlDouble(e);
            actual = System.Math.Round((double)UserDefinedFunctions.Student_InverseLeftProbability(n, p), r);

            Assert.AreEqual(expected, actual, "Failed at e = " + e.ToString());

            e = 2.228;
            n = 10.0;
            p = 95.0;
            r = 3;

            expected = new SqlDouble(e);
            actual = System.Math.Round((double)UserDefinedFunctions.Student_InverseLeftProbability(n, p), r);

            Assert.AreEqual(expected, actual, "Failed at e = " + e.ToString());

            e = 2.750;
            n = 30.0;
            p = 99.0;
            r = 3;

            expected = new SqlDouble(e);
            actual = System.Math.Round((double)UserDefinedFunctions.Student_InverseLeftProbability(n, p), r);

            Assert.AreEqual(expected, actual, "Failed at e = " + e.ToString());

        }
    }
}
