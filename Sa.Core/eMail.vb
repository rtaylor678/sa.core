﻿
Namespace eMail

    ''' <summary>
    ''' E-mail address and host name verification.
    ''' </summary>
    ''' <remarks></remarks>
    Public Module Address

        ''' <summary>
        ''' Verifies the e-mail address is well formed.
        ''' </summary>
        ''' <param name="eMailAddress">Well formed e-mail address.</param>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Public Function ValidateAddress(ByVal eMailAddress As String) As Boolean

            Dim bValidateAddress As Boolean = False

            Try

                Dim a As New System.Net.Mail.MailAddress(eMailAddress)

                bValidateAddress = CType(a.Address = eMailAddress, Boolean)

            Catch ex As Exception
                Sa.Core.Log.Write(ex.Message, EventLogEntryType.FailureAudit, False, Sa.Core.Log.LogName, 0)

            End Try

            Return bValidateAddress

        End Function

        ''' <summary>
        ''' Verifies the e-mail address is well formed and the e-mail host exists.
        ''' </summary>
        ''' <param name="eMailAddress">Well formed e-mail address.</param>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Public Function ValidateAddressHost(ByVal eMailAddress As String) As Boolean

            Dim bValidateAddressAndHost As Boolean = False

            Try

                Dim a As New System.Net.Mail.MailAddress(eMailAddress)

                bValidateAddressAndHost = Sa.Core.eMail.Address.ValidateHost(a.Host)

            Catch ex As Exception
                Sa.Core.Log.Write(ex.Message, EventLogEntryType.FailureAudit, False, Sa.Core.Log.LogName, 0)

            End Try

            Return bValidateAddressAndHost

        End Function

        ''' <summary>
        ''' Validates that the host exists using a DNS lookup function.
        ''' </summary>
        ''' <param name="HostName">String representing the e-mail server host name.</param>
        ''' <returns>Boolean</returns>
        ''' <remarks></remarks>
        Friend Function ValidateHost(ByVal HostName As String) As Boolean

            Dim bValidateHost As Boolean = False

            Try

                Dim d As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(HostName)

                bValidateHost = CType(d.AddressList.Length > 0, Boolean)

            Catch ex As Exception
                Sa.Core.Log.Write(ex.Message, EventLogEntryType.FailureAudit, False, Sa.Core.Log.LogName, 0)

            End Try

            Return bValidateHost

        End Function

    End Module

End Namespace
